import java.util.*;

public class Episode {
	
	private String nameOfEpisode;
	private int lengthOfEpisode;
	private int numberOfEpisode;
	private int releaseDate;

    public int getLengthOfEpisode() {
        return lengthOfEpisode;
    }

    public String getNameOfEpisode() {
        return nameOfEpisode;
    }

    public int getNumberOfEpisode() {
        return numberOfEpisode;
    }

    public int getReleaseDate() {
        return releaseDate;
    }

    public void setLengthOfEpisode(int lengthOfEpisode) {
        this.lengthOfEpisode = lengthOfEpisode;
    }

    public void setNameOfEpisode(String nameOfEpisode) {
        this.nameOfEpisode = nameOfEpisode;
    }

    public void setNumberOfEpisode(int numberOfEpisode) {
        this.numberOfEpisode = numberOfEpisode;
    }

    public void setReleaseDate(int releaseDate) {
        this.releaseDate = releaseDate;
    }
	
	
}