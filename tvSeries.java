
import java.util.*;

public class tvSeries {
	
	private String name;
	private Season season;
	private int numberOfSeasons;
	private Date yearOfRelease;

    public String getName() {
        return name;
    }

    public int getNumberOfSeasons() {
        return numberOfSeasons;
    }

    public Season getSeason() {
        return season;
    }

    public Date getYearOfRelease() {
        return yearOfRelease;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumberOfSeasons(int numberOfSeasons) {
        this.numberOfSeasons = numberOfSeasons;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public void setYearOfRelease(Date yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }
	
	

}