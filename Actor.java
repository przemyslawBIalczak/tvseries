
import java.util.*;

public class Actor {

	private String name;
	private String surname;
	private Date DateOfBirth;
	private String biography;

    public String getBiography() {
        return biography;
    }

    public Date getDateOfBirth() {
        return DateOfBirth;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public void setDateOfBirth(Date DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
        
        

}

